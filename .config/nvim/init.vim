" disable the vi compatible mode
set nocompatible

" enable persistent undo files
set undodir=$HOME/.local/share/nvim/persistent-undo
set undofile

" enable auto-indentation
filetype plugin indent on

" enable syntax highlighting
syntax enable

" set the tab width to 4 and make sure spaces are used instead of the tab
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab smarttab autoindent

" enable saner search defaults
set incsearch ignorecase smartcase hlsearch

" set the encoding to UTF8
set encoding=utf-8

" enable line numbers
set number

" enable window title name
set title
